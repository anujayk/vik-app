var myApp = angular.module('myApp',['myFactory']);

myApp.controller('myCtrl',['$scope','myFactory',function($scope,myFactory){

	$scope.adds = myFactory.dataContainer();

}]);