var fact = angular.module('myFactory',[]);

var resource  = function(){}
resource.prototype.dataContainer = function(){

	return [
			{
				"title"		:"sample name",
				"days_left" :"07",
				"time_left" :"H:i:s",
				"location"  :"Delhi",
				"banner"    :"img/img1.jpg"  
		   },
		   {
				"title"		:"sample header",
				"days_left" :"06",
				"time_left" :"H:i:s",
				"location"  :"Gurgaon",
				"banner"    :"img/img2.jpg"  
		   },
		   {
				"title"		:"header name",
				"days_left" :"07",
				"time_left" :"H:i:s",
				"location"  :"noida",
				"banner"    :"img/img3.jpg"  
		   },
		   {
				"title"		:"header simple",
				"days_left" :"06",
				"time_left" :"H:i:s",
				"location"  :"faridabad",
				"banner"    :"img/img4.jpg"  
		   }
		   ]; 	

}
fact.factory('myFactory',function(){

	return new resource;

});
